/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.services;

import com.nexos.java.people.models.Audit;
import com.nexos.java.people.repositories.AuditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author olmemarin
 */

@Service
public class AuditService {
    
    @Autowired
    AuditRepository auditRepository;
    
    public Audit add (Audit audit){
        return auditRepository.save(audit);
    }
}
