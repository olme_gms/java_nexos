/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.services;

import com.nexos.java.people.models.DocumentType;
import com.nexos.java.people.repositories.DocumentTypeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author olmemarin
 */

@Service
public class DocumentTypeService {
    
    @Autowired
    DocumentTypeRepository documentTypeRepository;
    
    public DocumentType add (DocumentType documentType) {
        
        return documentTypeRepository.save(documentType);
    }
    
    public DocumentType get (int id) {
        return documentTypeRepository.get(id);
    }
    
    public List<DocumentType> all () {
        return documentTypeRepository.findAll();
    }
    
    public int list (int id) {
        return documentTypeRepository.destroy(id);
    }
    
    public int delete (int id) {
        return documentTypeRepository.delete(id);
    }
    
}
