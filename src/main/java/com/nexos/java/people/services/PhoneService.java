/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.services;

import com.nexos.java.people.models.Phone;
import com.nexos.java.people.models.PhoneType;
import com.nexos.java.people.repositories.PhoneRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author olmemarin
 */

@Service
public class PhoneService {
    
    @Autowired
    PhoneRepository phoneRepository;
    
    public Phone add (Phone phone) {
        return phoneRepository.save(phone);
    }
    
    public Phone get (int id) {
        return phoneRepository.get(id);
    }
    
    public List<Phone> all () {
        return phoneRepository.findAll();
    }
    
    public int update (int id) {
        return phoneRepository.update(id);
    }
    
    public int delete (int id) {
        return phoneRepository.delete(id);
    } 
    
}
