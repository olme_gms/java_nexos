/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.services;

import com.nexos.java.people.models.PhoneType;
import com.nexos.java.people.repositories.PhoneTypeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author olmemarin
 */

@Service
public class PhoneTypeService {
    
    @Autowired
    PhoneTypeRepository phoneTypeRepository;
    
    public PhoneType add (PhoneType phoneType) {
        return phoneTypeRepository.save(phoneType);
    }
    
    public PhoneType get (int id) {
        return phoneTypeRepository.get(id);
    }
    
    public List<PhoneType> all () {
        return phoneTypeRepository.findAll();
    }
    
    public int update (int id) {
        return phoneTypeRepository.update(id);
    }
    
    public int delete (int id) {
        return phoneTypeRepository.delete(id);
    } 
    
}
