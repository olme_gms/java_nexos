/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.services;

import com.nexos.java.people.models.PhoneType;
import com.nexos.java.people.models.Relationship;
import com.nexos.java.people.repositories.RelationshipRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 *
 * @author olmemarin
 */

@Service
public class RelationshipService {
    
    @Autowired
    RelationshipRepository relationshipRepository;
    
    public Relationship add (Relationship relationship) {
        return relationshipRepository.save(relationship);
    }
    
    public Relationship get (int id) {
        return relationshipRepository.get(id);
    }
    
    public List<Relationship> all () {
        return relationshipRepository.findAll();
    }
    
    public int update (int id) {
        return relationshipRepository.update(id);
    }
    
    public int delete (int id) {
        return relationshipRepository.delete(id);
    } 
}
