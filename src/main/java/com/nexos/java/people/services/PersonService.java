/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.services;

import com.nexos.java.people.models.Person;
import com.nexos.java.people.repositories.PersonRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author olmemarin
 */

@Service
public class PersonService {
    
    @Autowired
    PersonRepository personRepository;
    
    public Person add (Person person) {
        return personRepository.save(person);
    }
    
    public Person get (int id) {
        return personRepository.get(id);
    }
    
    public List<Person> all () {
        return personRepository.findAll();
    }
    
    public int update (int id) {
        return personRepository.update(id);
    }
    
    public int delete (int id) {
        return personRepository.delete(id);
    } 
    
}
