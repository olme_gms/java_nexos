/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.services;

import com.nexos.java.people.models.Family;
import com.nexos.java.people.repositories.FamilyRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author olmemarin
 */

@Service
public class FamilyService {
    @Autowired
    FamilyRepository familyRepository;
    
    public Family add (Family family) {
        return familyRepository.save(family);
    }
    
    public Family get (int id) {
        return familyRepository.get(id);
    }
    
    public List<Family> all () {
        return familyRepository.findAll();
    }
    
    public int update (int id) {
        return familyRepository.update(id);
    }
    
    public int delete (int id) {
        return familyRepository.delete(id);
    }
}
