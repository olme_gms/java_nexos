/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.controllers;

import com.nexos.java.people.models.Audit;
import com.nexos.java.people.models.PhoneType;
import com.nexos.java.people.models.Relationship;
import com.nexos.java.people.services.AuditService;
import com.nexos.java.people.services.RelationshipService;
import com.nexos.java.people.util.PeopleUtil;
import com.nexos.java.people.vo.RelationshipInput;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author olmemarin
 */

@Controller
@RequestMapping("/api")
public class RelationshipController {
    
    @Autowired
    AuditService auditService;
    
    @Autowired
    RelationshipService relationshipService;
    
    @PostMapping("/save_relationship")
    public ResponseEntity savePhoneType(@Valid @RequestBody RelationshipInput relationshipInput){
        ResponseEntity response;
        try {
            
            Relationship relationship = new Relationship();
            relationship.setState(true);
            relationship.setName(relationshipInput.getName());
            Relationship newRelationship = relationshipService.add(relationship);
            if(newRelationship != null) {
               Map jsonDt = PeopleUtil.parametersMap(newRelationship);
               jsonDt.get(newRelationship);
                Audit audit = PeopleUtil.addAudit(relationshipInput, jsonDt, "Se una relación");
                Audit newAudit = auditService.add(audit);
               Map<String, Object> data =  PeopleUtil.successHttpApi(true, "phone_type", jsonDt);
               response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
               response = PeopleUtil.errorHttpApi("Lo sentimos no se pudo agregar la relación");
            }
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        
        return response;
    }
    
    @GetMapping("/relationships")
    public ResponseEntity getRelationships() {
        ResponseEntity response;
        
        try {
            List<Relationship> relationship = relationshipService.all();
            Audit audit = PeopleUtil.addAudit("", relationship, "Se enlista las relaciones");
            Audit newAudit = auditService.add(audit);
            Map<String, Object> data =  PeopleUtil.successHttpApi(true, "relationships", relationship);
            response = ResponseEntity.status(HttpStatus.OK).body(data);
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        
        return response;
    }
    
    @GetMapping("/relationship/{id}")
    public ResponseEntity getRelationship(@NotNull @PathVariable int id) {
        ResponseEntity response;
        
        try {
            Relationship relationship = relationshipService.get(id);
            Audit audit = PeopleUtil.addAudit(id, relationship, "Se trae un tipo de relación");
            Audit newAudit = auditService.add(audit);
            Map<String, Object> data =  PeopleUtil.successHttpApi(true, "relationship", relationship);
            response = ResponseEntity.status(HttpStatus.OK).body(data);  
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        
        return response;
    }
    
    @PutMapping("/relationship/{id}")
    public ResponseEntity putRelationship (@NotNull @PathVariable int id, @Valid @RequestBody RelationshipInput relationshipInput) {
        ResponseEntity response;
        try {
            Relationship relationship = relationshipService.get(id);
            relationship.setName(relationshipInput.getName());
            Relationship newRelationship = relationshipService.add(relationship);
            if(newRelationship != null){
               Map jsonDt = PeopleUtil.parametersMap(newRelationship);
               jsonDt.get(newRelationship);
               Audit audit = PeopleUtil.addAudit(relationshipInput, newRelationship, "Se actualizo la relación");
               Audit newAudit = auditService.add(audit);
               Map<String, Object> data =  PeopleUtil.successHttpApi(true, "phone_type", jsonDt);
               response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
                response = PeopleUtil.errorHttpApi("Lo sentimos no se pudo actualizar la relación");
            }
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        return response;
    }
    
    @GetMapping("/destroy_relationship/{id}")
    public ResponseEntity destroyRelationship(@NotNull @PathVariable int id) {
        ResponseEntity response;
        try {
            
            Relationship relationship = relationshipService.get(id);
            relationship.setState(false);
            Relationship newRelationship = relationshipService.add(relationship);
            if(newRelationship != null){
               Map jsonDt = PeopleUtil.parametersMap(newRelationship);
               jsonDt.get(newRelationship);
               Audit audit = PeopleUtil.addAudit(id, newRelationship, "Se elimino la relacioón");
               Audit newAudit = auditService.add(audit);
               Map<String, Object> data =  PeopleUtil.successHttpApi(true, "relationship", jsonDt);
               response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
                response = PeopleUtil.errorHttpApi("Lo sentimos no se pudo eliminar el tipo de teléfono");
            }
            
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        
        return response;
    }
    
    @DeleteMapping("/relationship/{id}")
    public ResponseEntity deleteRelationship(@NotNull @PathVariable int id){
        ResponseEntity response;
         try {
            int delete = relationshipService.delete(id);
          
            if(delete != 0){
              Audit audit = PeopleUtil.addAudit(id, "Se elimino", "Se destruyo la relación");
              Audit newAudit = auditService.add(audit);
              Map<String, Object> data =  PeopleUtil.successHttpApi(true, "relationship", "Se elimino ");
              response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
              response = PeopleUtil.errorHttpApi("Lo sentimos no pudimos borrar la relación");
            }
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        return response; 
    }
    
}
