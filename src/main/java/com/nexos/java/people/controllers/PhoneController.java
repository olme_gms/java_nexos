/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.controllers;

import com.nexos.java.people.models.Audit;
import com.nexos.java.people.models.Phone;
import com.nexos.java.people.models.PhoneType;
import com.nexos.java.people.services.AuditService;
import com.nexos.java.people.services.PhoneService;
import com.nexos.java.people.util.PeopleUtil;
import com.nexos.java.people.vo.PhoneInput;
import com.nexos.java.people.vo.PhoneTypeInput;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author olmemarin
 */

@Controller
@RequestMapping("/api")
public class PhoneController {
    
    @Autowired
    AuditService auditService;
    
    @Autowired
    PhoneService phoneService;
    
    @PostMapping("/save_phone")
    public ResponseEntity savePhoneType(@Valid @RequestBody PhoneInput phoneInput){
        
        ResponseEntity response;
        
        try {
            Phone phone = new Phone();
            phone.setIdPhoneType(phoneInput.getIdPhoneType());
            phone.setIdPerson(phoneInput.getIdPerson());
            phone.setPhone(phoneInput.getPhone());
            phone.setExtension(phoneInput.getExtension());
            Phone newPhone = phoneService.add(phone);
            
            if(newPhone != null) {
               Map jsonDt = PeopleUtil.parametersMap(newPhone);
               jsonDt.get(newPhone);
                Audit audit = PeopleUtil.addAudit(phoneInput, jsonDt, "Se agrega teléfono");
                Audit newAudit = auditService.add(audit);
               Map<String, Object> data =  PeopleUtil.successHttpApi(true, "phone", jsonDt);
               response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
               response = PeopleUtil.errorHttpApi("Lo sentimos no se pudo agregar el teléfono");
            }
            
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        
        return response;
    
    }
    
   @GetMapping("/phones")
    public ResponseEntity listPhones() {

        ResponseEntity response;
        try {
            List<Phone> phone = phoneService.all();
            Audit audit = PeopleUtil.addAudit("", phone, "Se enlista teléfonos");
            Audit newAudit = auditService.add(audit);
            Map<String, Object> data =  PeopleUtil.successHttpApi(true, "phone", phone);
            response = ResponseEntity.status(HttpStatus.OK).body(data);
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }

        return response;
    }
    
    @GetMapping("/phone/{id}")
    public ResponseEntity getPhoneType(@NotNull @PathVariable int id) {
        ResponseEntity response;
        
        try {
            Phone phone = phoneService.get(id);
            Audit audit = PeopleUtil.addAudit(id, phone, "Se trae un teléfono");
            Audit newAudit = auditService.add(audit);
            Map<String, Object> data =  PeopleUtil.successHttpApi(true, "phone", phone);
            response = ResponseEntity.status(HttpStatus.OK).body(data);  
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        
        return response;
    }
    
    @PutMapping("/phone/{id}")
    public ResponseEntity putPhone(@NotNull @PathVariable int id, @Valid @RequestBody PhoneInput phoneInput) {
        ResponseEntity response;
        try {
            Phone phone = phoneService.get(id);
            phone.setIdPhoneType(phoneInput.getIdPhoneType());
            phone.setIdPerson(phoneInput.getIdPerson());
            phone.setPhone(phoneInput.getPhone());
            phone.setExtension(phoneInput.getExtension());
            Phone newPhone = phoneService.add(phone);
            
            
            if(newPhone != null){
               Map jsonDt = PeopleUtil.parametersMap(newPhone);
               jsonDt.get(newPhone);
               Audit audit = PeopleUtil.addAudit(phoneInput, newPhone, "Se actualizo un teléfono");
               Audit newAudit = auditService.add(audit);
               Map<String, Object> data =  PeopleUtil.successHttpApi(true, "phone", jsonDt);
               response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
                response = PeopleUtil.errorHttpApi("Lo sentimos no se pudo actualizar el teléfono");
            }
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        return response;
    }
    
    @GetMapping("/delete_phone/{id}")
    public ResponseEntity deletePhone(@NotNull @PathVariable int id) {
        ResponseEntity response;
        try {
            
            Phone phone = phoneService.get(id);
            phone.setState(false);
            Phone newPhone = phoneService.add(phone);
            if(newPhone != null){
               Map jsonDt = PeopleUtil.parametersMap(newPhone);
               jsonDt.get(newPhone);
               Audit audit = PeopleUtil.addAudit(id, newPhone, "Se elimino un  teléfono");
               Audit newAudit = auditService.add(audit);
               Map<String, Object> data =  PeopleUtil.successHttpApi(true, "phone", jsonDt);
               response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
                response = PeopleUtil.errorHttpApi("Lo sentimos no se pudo eliminar el teléfono");
            }
            
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        
        return response;
    }
    
    @DeleteMapping("/phone/{id}")
    public ResponseEntity destroyDocumentType(@NotNull @PathVariable int id){
        ResponseEntity response;
        try {
            int delete = phoneService.delete(id);
          
            if(delete != 0){
              Audit audit = PeopleUtil.addAudit(id, "Se elimino", "Se destruyo un teléfono");
              Audit newAudit = auditService.add(audit);
              Map<String, Object> data =  PeopleUtil.successHttpApi(true, "phone", "Se elimino ");
              response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
              response = PeopleUtil.errorHttpApi("Lo sentimos no pudimos borrar el teléfono");
            }
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        return response;
    }
    
}
