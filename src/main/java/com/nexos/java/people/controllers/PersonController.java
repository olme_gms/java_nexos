/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.controllers;

import com.nexos.java.people.models.Audit;
import com.nexos.java.people.models.Person;
import com.nexos.java.people.models.PhoneType;
import com.nexos.java.people.services.AuditService;
import com.nexos.java.people.services.PersonService;
import com.nexos.java.people.util.PeopleUtil;
import com.nexos.java.people.vo.PersonInput;
import com.nexos.java.people.vo.PhoneTypeInput;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author olmemarin
 */

@Controller
@RequestMapping("/api")
public class PersonController {
    
    @Autowired
    AuditService auditService;
    
    @Autowired
    PersonService personService;
    
    @PostMapping("/save_person")
    public ResponseEntity savePhoneType(@Valid @RequestBody PersonInput personInput){
        
        ResponseEntity response;
        try {
            DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date convertido = fechaHora.parse(personInput.getBirthday());
            Person person = new Person();
            person.setIdDocumentType(personInput.getIdDocumentType());
            person.setDocument(personInput.getDocument());
            person.setFirstName(personInput.getFirstName());
            person.setSecondName(personInput.getSecondName());
            person.setFirstSurname(personInput.getFirstSurname());
            person.setSecondSurname(personInput.getSecondSurname());
            person.setBirthday(convertido);
            person.setState(true);
            
            Person newPerson = personService.add(person);
            if(newPerson != null) {
               Map jsonDt = PeopleUtil.parametersMap(newPerson);
               jsonDt.get(newPerson);
                Audit audit = PeopleUtil.addAudit(newPerson, jsonDt, "Se agrega una persona");
                Audit newAudit = auditService.add(audit);
               Map<String, Object> data =  PeopleUtil.successHttpApi(true, "person", jsonDt);
               response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
               response = PeopleUtil.errorHttpApi("Lo sentimos no se pudo agregar el tipo de documento");
            }            
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        return response;
    }
        
    @GetMapping("/people")
     public ResponseEntity people() {

        ResponseEntity response;
        try {
            List<Person> people = personService.all();
            Audit audit = PeopleUtil.addAudit("", people, "Se enlista las personas");
            Audit newAudit = auditService.add(audit);
            Map<String, Object> data =  PeopleUtil.successHttpApi(true, "people", people);
            response = ResponseEntity.status(HttpStatus.OK).body(data);
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }

        return response;
    }
     
    @GetMapping("/person/{id}")
    public ResponseEntity getPerson(@NotNull @PathVariable int id) {
        ResponseEntity response;
        
        try {
            Person person = personService.get(id);
            Audit audit = PeopleUtil.addAudit(id, person, "Se trae una persona");
            Audit newAudit = auditService.add(audit);
            Map<String, Object> data =  PeopleUtil.successHttpApi(true, "person", person);
            response = ResponseEntity.status(HttpStatus.OK).body(data);  
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        
        return response;
    }
    
    @PutMapping("/person/{id}")
    public ResponseEntity putPerson(@NotNull @PathVariable int id, @Valid @RequestBody PersonInput personInput) {
        ResponseEntity response;
        try {
            Person person = personService.get(id);
            DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date convertido = fechaHora.parse(personInput.getBirthday());
            person.setIdDocumentType(personInput.getIdDocumentType());
            person.setDocument(personInput.getDocument());
            person.setFirstName(personInput.getFirstName());
            person.setSecondName(personInput.getSecondName());
            person.setFirstSurname(personInput.getFirstSurname());
            person.setSecondSurname(personInput.getSecondSurname());
            person.setBirthday(convertido);
            person.setState(true);
            Person newPerson = personService.add(person);
            
            if(newPerson != null){
               Map jsonDt = PeopleUtil.parametersMap(newPerson);
               jsonDt.get(newPerson);
               Audit audit = PeopleUtil.addAudit(personInput, newPerson, "Se actualizo una persona");
               Audit newAudit = auditService.add(audit);
               Map<String, Object> data =  PeopleUtil.successHttpApi(true, "person", jsonDt);
               response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
                response = PeopleUtil.errorHttpApi("Lo sentimos no se pudo actualizar el tipo de teléfono");
            }
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        return response;
    }
    
    @GetMapping("/delete_person/{id}")
    public ResponseEntity deletePerson(@NotNull @PathVariable int id) {
        ResponseEntity response;
        try {
            
            Person person = personService.get(id);
            person.setState(false);
            Person newPerson = personService.add(person);
            if(newPerson != null){
               Map jsonDt = PeopleUtil.parametersMap(newPerson);
               jsonDt.get(newPerson);
               Audit audit = PeopleUtil.addAudit(id, newPerson, "Se elimino una persona");
               Audit newAudit = auditService.add(audit);
               Map<String, Object> data =  PeopleUtil.successHttpApi(true, "person", jsonDt);
               response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
                response = PeopleUtil.errorHttpApi("Lo sentimos no se pudo eliminar la persona");
            }
            
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        return response;
    }    
    
    @DeleteMapping("/person/{id}")
    public ResponseEntity destroyPerson(@NotNull @PathVariable int id){
        ResponseEntity response;
        try {
            int delete = personService.delete(id);
          
            if(delete != 0){
              Audit audit = PeopleUtil.addAudit(id, "Se elimino", "Se destruyo la persona");
              Audit newAudit = auditService.add(audit);
              Map<String, Object> data =  PeopleUtil.successHttpApi(true, "person", "Se elimino ");
              response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
              response = PeopleUtil.errorHttpApi("Lo sentimos no pudimos borrar la persona");
            }
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        return response;
    }
      
    
}
