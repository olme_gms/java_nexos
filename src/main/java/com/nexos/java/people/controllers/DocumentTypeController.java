/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.controllers;

import com.nexos.java.people.models.DocumentType;
import com.nexos.java.people.services.DocumentTypeService;
import com.nexos.java.people.util.PeopleUtil;
import com.nexos.java.people.vo.DocumentTypeInput;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author olmemarin
 */

@Controller
@RequestMapping("/api")
public class DocumentTypeController {
    
    @Autowired
    DocumentTypeService documentTypeService;
    
    
    @PostMapping("/save_document_type")
    public ResponseEntity saveDocumentType(@Valid @RequestBody DocumentTypeInput documentTypeInput) {
    
        ResponseEntity response;
        try {
            
            // Se agrega el tipo de documento
            DocumentType documentType = new DocumentType();
            documentType.setState(true);
            documentType.setDocumentType(documentTypeInput.getDocument_type());
            DocumentType newDocumentType = documentTypeService.add(documentType);
            if(newDocumentType != null) {
               Map jsonDt = PeopleUtil.parametersMap(newDocumentType);
               jsonDt.get(newDocumentType);
               Map<String, Object> data =  PeopleUtil.successHttpApi(true, "document_type", jsonDt);
               response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
               response = PeopleUtil.errorHttpApi("Lo sentimos no se pudo agregar el tipo de documento");
            }

        } catch (Exception e) {
           response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        
        return response;
    }
    
    @GetMapping("/document_types")
    public ResponseEntity listDocumentType() {
        ResponseEntity response;
        try {
            List<DocumentType> documentType = documentTypeService.all();
            Map<String, Object> data =  PeopleUtil.successHttpApi(true, "document_types", documentType);
            response = ResponseEntity.status(HttpStatus.OK).body(data);
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        return response;
    }
    
    @GetMapping("/document_type/{id}")
    public ResponseEntity getDocumentType(@NotNull @PathVariable int id) {
        ResponseEntity response;
        try {
            DocumentType documentType = documentTypeService.get(id);
            Map<String, Object> data =  PeopleUtil.successHttpApi(true, "document_type", documentType);
            response = ResponseEntity.status(HttpStatus.OK).body(data);
            
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        
        
        return response;
    }
    
    @PutMapping("/document_type/{id}")
    public ResponseEntity putDocumentType(@NotNull @PathVariable int id, @Valid @RequestBody DocumentTypeInput documentTypeInput) {
        ResponseEntity response;
        try {
            DocumentType documentType = documentTypeService.get(id);
            documentType.setDocumentType(documentTypeInput.getDocument_type());
             DocumentType newDocumentType = documentTypeService.add(documentType);
            if(newDocumentType != null) {
               Map jsonDt = PeopleUtil.parametersMap(newDocumentType);
               jsonDt.get(newDocumentType);
               Map<String, Object> data =  PeopleUtil.successHttpApi(true, "document_type", jsonDt);
               response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
               response = PeopleUtil.errorHttpApi("Lo sentimos no se pudo actualizar el tipo de documento");
            }
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        
        return response;
    }
    
    @GetMapping("/destroy_document_type/{id}")
    public ResponseEntity destroyDocumentType(@NotNull @PathVariable int id) {
        ResponseEntity response;
        try {
            DocumentType documentType = documentTypeService.get(id);
            documentType.setState(false);
             DocumentType newDocumentType = documentTypeService.add(documentType);
            if(newDocumentType != null) {
               Map jsonDt = PeopleUtil.parametersMap(newDocumentType);
               jsonDt.get(newDocumentType);
               Map<String, Object> data =  PeopleUtil.successHttpApi(true, "document_type", jsonDt);
               response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
               response = PeopleUtil.errorHttpApi("Lo sentimos no se pudo eliminar el tipo de documento");
            }
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        
        return response;
    }
    
    @DeleteMapping("/document_type/{id}")
    public ResponseEntity deleteDocumentType(@NotNull @PathVariable int id){
        ResponseEntity response;
        try {
            
          int delete = documentTypeService.delete(id);
          
          if(delete != 0){
            Map<String, Object> data =  PeopleUtil.successHttpApi(true, "document_type", "Se elimino ");
            response = ResponseEntity.status(HttpStatus.OK).body(data);
          } else {
            response = PeopleUtil.errorHttpApi("Lo sentimos no pudimos borrar el documento");
          }
          
            
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        return response;
    }
    
}
