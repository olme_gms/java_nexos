/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.controllers;

import com.nexos.java.people.models.Audit;
import com.nexos.java.people.models.PhoneType;
import com.nexos.java.people.services.AuditService;
import com.nexos.java.people.services.PhoneTypeService;
import com.nexos.java.people.util.PeopleUtil;
import com.nexos.java.people.vo.PhoneTypeInput;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author olmemarin
 */

@Controller
@RequestMapping("/api")
public class PhoneTypeController {
    
    @Autowired
    PhoneTypeService phoneTypeservice;
    
    @Autowired
    AuditService auditService;
    
    @PostMapping("/save_phone_type")
    public ResponseEntity savePhoneType(@Valid @RequestBody PhoneTypeInput phoneTypeInput){
        
        ResponseEntity response;
        
        try {
            PhoneType phoneType = new PhoneType();
            phoneType.setState(true);
            phoneType.setPhoneType(phoneTypeInput.getPhone_type());
            PhoneType newPhoneType = phoneTypeservice.add(phoneType);
            if(newPhoneType != null) {
               Map jsonDt = PeopleUtil.parametersMap(newPhoneType);
               jsonDt.get(newPhoneType);
                Audit audit = PeopleUtil.addAudit(phoneTypeInput, jsonDt, "Se agrega tipo de teléfono");
                Audit newAudit = auditService.add(audit);
               Map<String, Object> data =  PeopleUtil.successHttpApi(true, "phone_type", jsonDt);
               response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
               response = PeopleUtil.errorHttpApi("Lo sentimos no se pudo agregar el tipo de documento");
            }
            
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        
        return response;
    }
    
    
    @GetMapping("/phone_types")
    public ResponseEntity listPhoneType() {

        ResponseEntity response;
        try {
            List<PhoneType> phoneType = phoneTypeservice.all();
            Audit audit = PeopleUtil.addAudit("", phoneType, "Se enlista tipo de teléfono");
            Audit newAudit = auditService.add(audit);
            Map<String, Object> data =  PeopleUtil.successHttpApi(true, "phone_types", phoneType);
            response = ResponseEntity.status(HttpStatus.OK).body(data);
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }

        return response;
    }
    
    @GetMapping("/phone_type/{id}")
    public ResponseEntity getPhoneType(@NotNull @PathVariable int id) {
        ResponseEntity response;
        
        try {
            PhoneType phoneType = phoneTypeservice.get(id);
            Audit audit = PeopleUtil.addAudit(id, phoneType, "Se trae un tipo de teléfono");
            Audit newAudit = auditService.add(audit);
            Map<String, Object> data =  PeopleUtil.successHttpApi(true, "document_type", phoneType);
            response = ResponseEntity.status(HttpStatus.OK).body(data);  
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        
        return response;
    }
    
    @PutMapping("/phone_type/{id}")
    public ResponseEntity putPhoneType(@NotNull @PathVariable int id, @Valid @RequestBody PhoneTypeInput phoneTypeInput) {
        ResponseEntity response;
        try {
            PhoneType phoneType = phoneTypeservice.get(id);
            phoneType.setPhoneType(phoneTypeInput.getPhone_type());
            PhoneType newPhoneType = phoneTypeservice.add(phoneType);
            if(newPhoneType != null){
               Map jsonDt = PeopleUtil.parametersMap(newPhoneType);
               jsonDt.get(newPhoneType);
               Audit audit = PeopleUtil.addAudit(phoneTypeInput, newPhoneType, "Se actualizo un tipo de teléfono");
               Audit newAudit = auditService.add(audit);
               Map<String, Object> data =  PeopleUtil.successHttpApi(true, "phone_type", jsonDt);
               response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
                response = PeopleUtil.errorHttpApi("Lo sentimos no se pudo actualizar el tipo de teléfono");
            }
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        return response;
    }
     
    @GetMapping("/destroy_phone_type/{id}")
    public ResponseEntity destroyPhoneType(@NotNull @PathVariable int id) {
        ResponseEntity response;
        try {
            
            PhoneType phoneType = phoneTypeservice.get(id);
            phoneType.setState(false);
            PhoneType newPhoneType = phoneTypeservice.add(phoneType);
            if(newPhoneType != null){
               Map jsonDt = PeopleUtil.parametersMap(newPhoneType);
               jsonDt.get(newPhoneType);
               Audit audit = PeopleUtil.addAudit(id, newPhoneType, "Se elimino un tipo de teléfono");
               Audit newAudit = auditService.add(audit);
               Map<String, Object> data =  PeopleUtil.successHttpApi(true, "phone_type", jsonDt);
               response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
                response = PeopleUtil.errorHttpApi("Lo sentimos no se pudo eliminar el tipo de teléfono");
            }
            
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        
        return response;
    }
    
    @DeleteMapping("/phone_type/{id}")
    public ResponseEntity deleteDocumentType(@NotNull @PathVariable int id){
        ResponseEntity response;
        try {
            int delete = phoneTypeservice.delete(id);
          
            if(delete != 0){
              Audit audit = PeopleUtil.addAudit(id, "Se elimino", "Se destruyo un tipo de teléfono");
              Audit newAudit = auditService.add(audit);
              Map<String, Object> data =  PeopleUtil.successHttpApi(true, "phone_type", "Se elimino ");
              response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
              response = PeopleUtil.errorHttpApi("Lo sentimos no pudimos borrar el tipo de teléfono");
            }
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        return response;
    }
    

    
}
