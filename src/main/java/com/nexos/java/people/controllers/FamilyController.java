/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.controllers;

import com.nexos.java.people.models.Audit;
import com.nexos.java.people.models.Family;
import com.nexos.java.people.services.AuditService;
import com.nexos.java.people.services.FamilyService;
import com.nexos.java.people.util.PeopleUtil;
import com.nexos.java.people.vo.FamilyInput;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author olmemarin
 */

@Controller
@RequestMapping("/api")
public class FamilyController {
    @Autowired
    AuditService auditService;
    
    @Autowired
    FamilyService familyService;
    
    @PostMapping("/save_family")
    public ResponseEntity savePhoneType(@Valid @RequestBody FamilyInput familyInput) {
        
        ResponseEntity response;
        
        try {
            DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date convertido = fechaHora.parse(familyInput.getBirthday());
            Family family = new Family();
            family.setIdPerson(familyInput.getIdPerson());
            family.setIdRelationship(familyInput.getIdRelationship());
            family.setIdDocumentType(familyInput.getIdDocumentType());
            family.setDocument(familyInput.getDocument());
            family.setFirstName(familyInput.getFirstName());
            family.setSencondName(familyInput.getSencondName());
            family.setFirstSurname(familyInput.getFirstSurname());
            family.setSecondSurname(familyInput.getSecondSurname());
            family.setBirthday(convertido);
            family.setState(true);
            
            Family newFamily = familyService.add(family);
            
            if(newFamily != null) {
               Map jsonDt = PeopleUtil.parametersMap(newFamily);
               jsonDt.get(newFamily);
                Audit audit = PeopleUtil.addAudit(familyInput, jsonDt, "Se agrega un familiar");
                Audit newAudit = auditService.add(audit);
               Map<String, Object> data =  PeopleUtil.successHttpApi(true, "family", jsonDt);
               response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
               response = PeopleUtil.errorHttpApi("Lo sentimos no se pudo agregar el tipo de documento");
            }
            
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        
        return response;
    }
    
    @GetMapping("/families")
    public ResponseEntity families() {

        ResponseEntity response;
        try {
            List<Family> families = familyService.all();
            Audit audit = PeopleUtil.addAudit("", families, "Se enlista los familiares");
            Audit newAudit = auditService.add(audit);
            Map<String, Object> data =  PeopleUtil.successHttpApi(true, "families", families);
            response = ResponseEntity.status(HttpStatus.OK).body(data);
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }

        return response;
    }
    
    @GetMapping("/family/{id}")
    public ResponseEntity getFamily(@NotNull @PathVariable int id) {
        ResponseEntity response;
        
        try {
            Family family = familyService.get(id);
            Audit audit = PeopleUtil.addAudit(id, family, "Se trae un familiar");
            Audit newAudit = auditService.add(audit);
            Map<String, Object> data =  PeopleUtil.successHttpApi(true, "family", family);
            response = ResponseEntity.status(HttpStatus.OK).body(data);  
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        
        return response;
    }
    
    @PutMapping("/family/{id}")
    public ResponseEntity putFamily(@NotNull @PathVariable int id, @Valid @RequestBody FamilyInput familyInput) {
        ResponseEntity response;
        try {
            DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date convertido = fechaHora.parse(familyInput.getBirthday());
            
            Family family = familyService.get(id);
            family.setIdPerson(familyInput.getIdPerson());
            family.setIdRelationship(familyInput.getIdRelationship());
            family.setIdDocumentType(familyInput.getIdDocumentType());
            family.setDocument(familyInput.getDocument());
            family.setFirstName(familyInput.getFirstName());
            family.setSencondName(familyInput.getSencondName());
            family.setFirstSurname(familyInput.getFirstSurname());
            family.setSecondSurname(familyInput.getSecondSurname());
            family.setBirthday(convertido);
            family.setState(true);
            
            Family newFamily = familyService.add(family);
            if(newFamily != null){
               Map jsonDt = PeopleUtil.parametersMap(newFamily);
               jsonDt.get(newFamily);
               Audit audit = PeopleUtil.addAudit(familyInput, newFamily, "Se actualizo el familiar");
               Audit newAudit = auditService.add(audit);
               Map<String, Object> data =  PeopleUtil.successHttpApi(true, "family", jsonDt);
               response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
                response = PeopleUtil.errorHttpApi("Lo sentimos no se pudo actualizar el familiar");
            }
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        return response;
    }
    
         
    @GetMapping("/delete_family/{id}")
    public ResponseEntity deleteFamily(@NotNull @PathVariable int id) {
        ResponseEntity response;
        try {
            
            Family family = familyService.get(id);
            family.setState(false);
            Family newFamily = familyService.add(family);
            if(newFamily != null){
               Map jsonDt = PeopleUtil.parametersMap(newFamily);
               jsonDt.get(newFamily);
               Audit audit = PeopleUtil.addAudit(id, newFamily, "Se elimino un familiar");
               Audit newAudit = auditService.add(audit);
               Map<String, Object> data =  PeopleUtil.successHttpApi(true, "family", jsonDt);
               response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
                response = PeopleUtil.errorHttpApi("Lo sentimos no se pudo eliminar el tipo de teléfono");
            }
            
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        
        return response;
    }
    
    @DeleteMapping("/family/{id}")
    public ResponseEntity destroyFamily(@NotNull @PathVariable int id){
        ResponseEntity response;
        try {
            int delete = familyService.delete(id);
          
            if(delete != 0){
              Audit audit = PeopleUtil.addAudit(id, "Se elimino", "Se destruyo el familiar");
              Audit newAudit = auditService.add(audit);
              Map<String, Object> data =  PeopleUtil.successHttpApi(true, "family", "Se elimino ");
              response = ResponseEntity.status(HttpStatus.OK).body(data);
            } else {
              response = PeopleUtil.errorHttpApi("Lo sentimos no pudimos borrar el familiar");
            }
        } catch (Exception e) {
            response = PeopleUtil.errorHttpApi(e.getMessage());
        }
        return response;
    }
}
