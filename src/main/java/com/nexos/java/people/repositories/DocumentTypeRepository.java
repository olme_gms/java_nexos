/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.repositories;

import com.nexos.java.people.models.DocumentType;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author olmemarin
 */

@Repository
public interface DocumentTypeRepository extends JpaRepository<DocumentType, Integer>{
    
    @Query(value = "SELECT * FROM db_family.document_type", nativeQuery = true)
    DocumentType all();
    
    @Query(value = "SELECT * FROM db_family.document_type WHERE state = 1", nativeQuery = true)
    DocumentType active();
    
    @Query(value = "SELECT * FROM db_family.document_type WHERE id = ?1", nativeQuery = true)
    DocumentType get(int id);
    
    @Modifying(clearAutomatically = true)
    @Transactional 
    @Query(value = "DELETE FROM db_family.document_type WHERE id = ?1", nativeQuery = true)
    int delete(int id);
    
    @Modifying(clearAutomatically = true)
    @Transactional 
    @Query(value = "UPDATE db_family.document_type SET state = 0 WHERE id = ?1", nativeQuery = true)
    int destroy(int id);
}
