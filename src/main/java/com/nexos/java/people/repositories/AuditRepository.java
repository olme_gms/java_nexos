/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.repositories;

import com.nexos.java.people.models.Audit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author olmemarin
 */

@Repository
public interface AuditRepository extends JpaRepository<Audit, Integer> {
    
}
