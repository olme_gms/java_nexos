/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.repositories;

import com.nexos.java.people.models.Phone;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author olmemarin
 */

@Repository
public interface PhoneRepository extends JpaRepository<Phone, Integer> {
    
    @Query(value = "SELECT * FROM db_family.phones", nativeQuery = true)
    Phone all();
    
    @Query(value = "SELECT * FROM db_family.phones WHERE state = 1", nativeQuery = true)
    Phone active();
    
    @Query(value = "SELECT * FROM db_family.phones WHERE id = ?1", nativeQuery = true)
    Phone get(int id);
    
    @Modifying(clearAutomatically = true)
    @Transactional 
    @Query(value = "DELETE FROM db_family.phones WHERE id = ?1", nativeQuery = true)
    int delete(int id);
    
    @Modifying(clearAutomatically = true)
    @Transactional 
    @Query(value = "UPDATE db_family.phones SET state = 0 WHERE id = ?1", nativeQuery = true)
    int update(int id);
    
}
