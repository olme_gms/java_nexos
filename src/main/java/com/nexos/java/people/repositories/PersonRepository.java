/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.repositories;

import com.nexos.java.people.models.Person;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author olmemarin
 */

@Repository
public interface PersonRepository extends JpaRepository<Person, Integer>{
    
    @Query(value = "SELECT * FROM db_family.person", nativeQuery = true)
    Person all();
    
    @Query(value = "SELECT * FROM db_family.person WHERE state = 1", nativeQuery = true)
    Person active();
    
    @Query(value = "SELECT * FROM db_family.person WHERE id = ?1", nativeQuery = true)
    Person get(int id);
    
    @Modifying(clearAutomatically = true)
    @Transactional 
    @Query(value = "DELETE FROM db_family.person WHERE id = ?1", nativeQuery = true)
    int delete(int id);
    
    @Modifying(clearAutomatically = true)
    @Transactional 
    @Query(value = "UPDATE db_family.person SET state = 0 WHERE id = ?1", nativeQuery = true)
    int update(int id);
    
}
