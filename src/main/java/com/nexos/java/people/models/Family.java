/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.models;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author olmemarin
 */

@Entity
@Table(name = "family")
public class Family {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id = 0;
    @Column(name = "id_person")
    private int idPerson = 0;
    @Column(name = "id_relationship")
    private int idRelationship = 0;
    @Column(name = "id_document_type")
    private int idDocumentType = 0;
    @Column(name = "document")
    private String document = null;
    @Column(name = "first_name")
    private String firstName = null;
    @Column(name = "second_name")
    private String sencondName = null;
    @Column(name = "first_surname")
    private String firstSurname = null;
    @Column(name = "second_surname")
    private String secondSurname = null;
    @Column(name = "birthday")
    private Date birthday = null;
    @Column(name = "state")
    private boolean state = false;

    public Family() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(int idPerson) {
        this.idPerson = idPerson;
    }

    public int getIdRelationship() {
        return idRelationship;
    }

    public void setIdRelationship(int idRelationship) {
        this.idRelationship = idRelationship;
    }

    public int getIdDocumentType() {
        return idDocumentType;
    }

    public void setIdDocumentType(int idDocumentType) {
        this.idDocumentType = idDocumentType;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSencondName() {
        return sencondName;
    }

    public void setSencondName(String sencondName) {
        this.sencondName = sencondName;
    }

    public String getFirstSurname() {
        return firstSurname;
    }

    public void setFirstSurname(String firstSurname) {
        this.firstSurname = firstSurname;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
    
    
}
