/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.util;

import com.google.gson.Gson;
import com.nexos.java.people.models.Audit;
import com.nexos.java.people.services.AuditService;
import com.nexos.java.people.vo.HttpErrorCustom;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 *
 * @author olmemarin
 */

@Component
public class PeopleUtil {
    
    @Autowired
    AuditService auditService;

    
    public static ResponseEntity errorHttpApi(String mensaje) {
        ResponseEntity response = null;
        HttpErrorCustom httpErrorCustom = HttpErrorCustom.builder().success(false).message(mensaje).build();
        return response = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(httpErrorCustom);
    }
    
    public static Map<String, Object>  successHttpApi( boolean success, String nameObject,Object object) {
        
        Map data = new HashMap();
        data.put("success", success);
        data.put(nameObject, object);
        
        return data;
    } 
    
    public static Map<String, Object> parametersMap(Object obj) {
        Map<String, Object> map = new HashMap<>();
        for (Field field : obj.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try { map.put(field.getName(), field.get(obj)); } catch (Exception e) { }
        }
        return map;
    }
    
    public static String obtenerIP() throws UnknownHostException {
        InetAddress ip = InetAddress.getLocalHost();
        return ip.getHostAddress();
    }
    
    public static Audit addAudit(Object obj, Object obj2, String accion) throws UnknownHostException {
        
        Gson gson = new Gson();
        Audit audit = new Audit();
        audit.setIp(obtenerIP());
        audit.setDataInput(gson.toJson(obj));
        audit.setDataOutput(gson.toJson(obj2));
        audit.setAction(accion);
        audit.setDate(new Date());
       
        return audit;
    }
}
