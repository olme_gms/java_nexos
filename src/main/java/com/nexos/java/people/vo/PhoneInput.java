/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.vo;

import javax.validation.constraints.NotNull;

/**
 *
 * @author olmemarin
 */
public class PhoneInput {
    @NotNull
    private int idPhoneType = 0;
    @NotNull
    private int idPerson = 0;
    @NotNull
    private String phone = null;
    private String extension = null;

    public int getIdPhoneType() {
        return idPhoneType;
    }

    public void setIdPhoneType(int idPhoneType) {
        this.idPhoneType = idPhoneType;
    }

    public int getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(int idPerson) {
        this.idPerson = idPerson;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
    
    
}
