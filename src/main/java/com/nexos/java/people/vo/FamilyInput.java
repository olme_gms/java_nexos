/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.vo;

import java.util.Date;
import javax.validation.constraints.NotNull;

/**
 *
 * @author olmemarin
 */
public class FamilyInput {
    @NotNull
    private int idPerson = 0;
    @NotNull
    private int idRelationship = 0;
    @NotNull
    private int idDocumentType = 0;
    @NotNull
    private String document = null;
    @NotNull
    private String firstName = null;
    private String sencondName = null;
    @NotNull
    private String firstSurname = null;
    private String secondSurname = null;
    @NotNull
    private String birthday = null;

    public int getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(int idPerson) {
        this.idPerson = idPerson;
    }

    public int getIdRelationship() {
        return idRelationship;
    }

    public void setIdRelationship(int idRelationship) {
        this.idRelationship = idRelationship;
    }

    public int getIdDocumentType() {
        return idDocumentType;
    }

    public void setIdDocumentType(int idDocumentType) {
        this.idDocumentType = idDocumentType;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSencondName() {
        return sencondName;
    }

    public void setSencondName(String sencondName) {
        this.sencondName = sencondName;
    }

    public String getFirstSurname() {
        return firstSurname;
    }

    public void setFirstSurname(String firstSurname) {
        this.firstSurname = firstSurname;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
    
    
    
}
