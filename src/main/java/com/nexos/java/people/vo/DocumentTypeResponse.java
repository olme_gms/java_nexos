/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.vo;

import com.nexos.java.people.models.DocumentType;
import java.lang.reflect.Array;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author olmemarin
 */

@Data
@Builder
public class DocumentTypeResponse {
    
    private boolean success = true;
    private Map document_type = null;
    
}
