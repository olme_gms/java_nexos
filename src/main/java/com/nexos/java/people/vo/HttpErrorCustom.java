/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.java.people.vo;

import lombok.Builder;
import lombok.Data;

/**
 *
 * @author olmemarin
 */
@Data
@Builder
public class HttpErrorCustom {
    
    private Boolean success = false;
    private String message = null;
}
